import { Pipe, PipeTransform } from '@angular/core';
import { ContextMenuItem } from '@app/modules/contextMenu/contextMenu.component';

@Pipe({
  name: 'countChildren',
})
export class CountChildrenPipe implements PipeTransform {

  private countChildren(item:ContextMenuItem<Node>) {
    let count = item.child?.length || 0;
    item.child?.forEach((child:ContextMenuItem<Node>) => {
      count = count + this.countChildren(child);
    });
    return count;
  }

  transform(item: ContextMenuItem<Node>): any {
    const total = this.countChildren(item);
    return total;
  }
}
