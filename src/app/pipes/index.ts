import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { CountChildrenPipe } from "./countChildren.pipe";

@NgModule({
  declarations:[
    CountChildrenPipe,
  ],
  imports:[CommonModule],
  exports:[
    CountChildrenPipe,
  ],
})
export class Pipes{}
