import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ContextMenuItem } from '@app/modules/contextMenu/contextMenu.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'reconeyez';
  contextMenuData!: ContextMenuItem<Node>[];

  constructor(
    private http: HttpClient,
  ) {}

  contextMenuClicked(event: boolean) {
    console.log('ContextMenu clicked');
  }

  ngOnInit() {
    this.http.get('/assets/mock.json').subscribe((data:any) => {
      this.contextMenuData = data;
    });
  }

}
