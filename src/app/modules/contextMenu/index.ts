import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { Pipes } from "@app/pipes";
import { ContextMenuComponent } from "./contextMenu.component";

@NgModule({
  declarations: [
    ContextMenuComponent,
  ],
  imports: [
    CommonModule,
    Pipes,
  ],
  exports: [
    ContextMenuComponent,
  ]
})

export class ContextMenuModule {}