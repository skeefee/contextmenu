import { Component, EventEmitter, HostBinding, HostListener, Input, OnInit, Output } from "@angular/core";

export interface ContextMenuItem<T> {
  child?: ContextMenuItem<T>[];
  label: string;
  data: T;
  active: boolean;
}

let contextMenuId = 0;

@Component({
  selector: 'contextMenu',
  templateUrl: 'contextMenu.template.html',
  styleUrls: ['contextMenu.styles.scss'],
})

export class ContextMenuComponent {
  @Input() data!: ContextMenuItem<Node>[];
  @Output() onClick: EventEmitter<boolean> = new EventEmitter();

  private id = `contextMenu-${contextMenuId++}`;
  public isVisible = false;

  @HostBinding('attr.id')
  get setHostId(): string {
    return this.id;
  }

  @HostListener('contextmenu', ['$event'])
  onRightClick(event: Event):void {
    event.preventDefault();
    this.isVisible = true;
    this.onClick.emit(true);
  }

  @HostListener('document:click', ['$event'])
  documentClick(event: PointerEvent): void {
    if (this.isVisible) {
      const parentElement = event.composedPath().filter((item:any) => {
        return item.id === this.id;
      });
      if (!parentElement.length) {
        this.isVisible = false;
      }
    }
  }
}